import farberkennung
import numpy as np

import io
from PIL import Image
import cv2
import keras
from keras import backend as K
from keras.models import Sequential
from keras.layers import Activation
from keras.layers.core import Dense, Flatten
from keras.optimizers import Adam, RMSprop
from keras.metrics import categorical_crossentropy
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing.image import img_to_array
from keras.layers.normalization import BatchNormalization
from keras.layers.convolutional import *
from keras.models import load_model

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras import applications


from keras.layers import Dense, GlobalAveragePooling2D
from keras.models import Model

from flask import Flask
from flask import request
from flask import jsonify
import base64

from keras import backend

import tensorflow as tf
from keras.backend.tensorflow_backend import set_session

config = tf.ConfigProto(
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)
    # device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
set_session(session)

farberkennung.hello_farben()

app = Flask(__name__)

def get_model():
	global model
	global arm_model
	global muster_model
	# GLOBAL DEFINIEREN!!
	model = load_model("./final_model.h5")
	arm_model = load_model("./arm_model.h5")
	muster_model = load_model("./mobileNet_32Dense_muster.h5")
	print(" * model loaded!")

def padding_that_image(img,target_size):


	return new_im

def preprocess_image(image, target_size):
	if image.mode != "RGB":
		image = image.convert("RGB")
	#image = padding_that_image(image,target_size)

	desired_size = target_size[0]
	img = np.array(image) 
	img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
	old_size = img.shape[:2] #np.array(img)
	ratio = float(desired_size)/max(old_size)
	new_size = tuple([int(x*ratio) for x in old_size])
	im = cv2.resize(img, (new_size[1], new_size[0]))
	delta_w = desired_size - new_size[1]	
	delta_h = desired_size - new_size[0]
	top, bottom = delta_h//2, delta_h-(delta_h//2)
	left, right = delta_w//2, delta_w-(delta_w//2)

	color = [255, 255, 255] # white padding_that_image
	image = cv2.copyMakeBorder(im, top, bottom, left, right, cv2.BORDER_CONSTANT,value=color)

	print("* hello")
	#image = image.resize(target_size)
	#if norm: 
	print("* * * * normalizing....")
	image = image.astype("float") / 255.0 #custom aaron
	print("* 2")
	image = img_to_array(image)
	print("* 3")
	image = np.expand_dims(image,axis=0)

	return image

def decode_base64(data):
    """Decode base64, padding being optional.

    :param data: Base64 data as an ASCII byte string
    :returns: The decoded byte string.

    """
    missing_padding = len(data) % 4
    if missing_padding != 0:
        data += b'='* (4 - missing_padding)
    return base64.decodestring(data)




@app.route('/dashboard', methods=["POST"]) #Send image data to end point for prediction
def predict():
	print("* called predict")
	message = request.get_json(force=True) #get data from POST
	encoded = message['image'] #access key 'image' -> image data is encoded in json
	print(encoded)
	encoded = encoded.replace("data:image/png;base64,","")

	decoded = base64.b64decode(encoded) # hence decode it!
	print("* * * * decoded")
	image = Image.open(io.BytesIO(decoded)) # here we create an Image-object and initialize is it our binarized image-data from before
	preprossed_image = preprocess_image(image, target_size=(224,224)) #now we can preprocess the image
	#preprossed_image_non_normed = preprocess_image(image, target_size=(224,224))
	prediction = model.predict(preprossed_image).tolist() # model is global and already inizialized at the top of this program
	arm_prediction = arm_model.predict(preprossed_image).tolist() # Armlängen-Modell
	muster_prediction = muster_model.predict(preprossed_image).tolist() # Armlängen-Modell

	print(muster_prediction)	

	## Convert PIL zu cv2 Format
	open_cv_image = np.array(image) 
	erkannte_farbe, rbg_farbwert = farberkennung.run_farberkennung(open_cv_image)
	#print("***** Erkannte Farbe lautet:", erkannte_farbe)
	#print("***** Erkannte Farbe lautet:", rbg_farbwert)

													# .tolist() converts the array to a python list (required for the jsonify)
	# create a dict holding a prediction dict													
	response = {
		# inside response we have another dict holding all the predictions		
		'prediction': {
				'34-hosen': prediction[0][0],
				 'Abendkleider':  prediction[0][1],
				 'Blusenkleider':  prediction[0][2],
				 'Brautkleider':  prediction[0][3],
				 'Bundfaltenhosen': prediction[0][4],
				 'Business-Hosen':  prediction[0][5],
				 'Chinos':  prediction[0][6],
				 'Cocktailkleider':  prediction[0][7],
				 'Cordhosen':  prediction[0][8],
				 'Culottes':  prediction[0][9],
				 'Etuikleider':  prediction[0][10],
				 'Haremshosen':  prediction[0][11],
				 'Jeanskleider':  prediction[0][12],
				 'Jerseykleider':  prediction[0][13],
				 'Lederhosen':  prediction[0][14],
				 'Leggings':  prediction[0][15],
				 'Marlenehosen':  prediction[0][16],
				 'Parka':  prediction[0][17],
				 'Shorts':  prediction[0][18],
				 'Sommerkleider':  prediction[0][19],
				 'Spitzenkleider':  prediction[0][20],
				 'Steppjacken':  prediction[0][21],
				 'Stoffhosen':  prediction[0][22],
				 'Strickkleider':  prediction[0][23],
				 'Sweatpants':  prediction[0][24],
				 'blousons':  prediction[0][25],
				 'bomberjacken':  prediction[0][26],
				 'cargojacken':  prediction[0][27],
				 'jeansjacken':  prediction[0][28],
				 'lederjacken':  prediction[0][29],
				 'outdoorjacken':  prediction[0][30],
				 'regenjacken':  prediction[0][31]
				 },
		'farbe':{'Top1': erkannte_farbe[0],
					'Top2': erkannte_farbe[1],
					'Top3': erkannte_farbe[2]
				},
		'rgb_farbwerte': {'Rgb-werte': rbg_farbwert},

		'arm_length': {

				'Ärmellos': arm_prediction[0][0],
				 'Dreiviertelarm':  arm_prediction[0][1],
				 'Halbarm':  arm_prediction[0][2],
				 'Langarm':  arm_prediction[0][3],
				 'Viertelarm': arm_prediction[0][4]
		},

		'muster': {

					'Animalprint': muster_prediction[0][0],
					'Camouflage': muster_prediction[0][1],
					'Ethno_Muster': muster_prediction[0][2],
					'Farbverlauf': muster_prediction[0][3],
					'Gebluemt_floral': muster_prediction[0][4],
					'Gepunktet': muster_prediction[0][5],
					'Gestreift': muster_prediction[0][6],
					'Kariert': muster_prediction[0][7],
					'Logoprint': muster_prediction[0][8],
					'Melange': muster_prediction[0][9],
					'Motivprint':muster_prediction[0][10],
					'Mottoprint':muster_prediction[0][11],
					'Norweger_Muster':muster_prediction[0][12],
					'Paisley_Muster':muster_prediction[0][13],
					'Two_Tone':muster_prediction[0][14],
					'Unifarben':muster_prediction[0][15]

		}
	}
	# results = imagenet_utils.decode_predictions(preds)
 #    data["predictions"] = []
	return jsonify(response)

if __name__ == '__main__':
	print("* Loading Keras...")
	get_model()
	app.run(debug=False, threaded=False)