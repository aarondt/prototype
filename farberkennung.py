# import the necessary packages
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import utils
import cv2
import utils

import numpy as np

from colormath.color_objects import sRGBColor, LabColor
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000


def hello_farben():
    print("******** Hello from farberkennung!")

color_dict = {
    "red": (255, 0, 0),
    "green": (0, 255, 0),
    "blue": (0, 0, 255),
     "braun": (160,82,45),
    "weiß": (255,255,255),
    "himmel-blau":(135,206,250),
    "schwarz":(0,0,0),
    "gelb": (255,255,0),
    "grau": (128,128,128),
    "dunkel-grün": (0,60,0),
    "darkolivegreen":(85,107,47),
    "dark-red":(195.0, 5.0, 37.0),

    'lightsalmon': (255,160,122),
    'salmon': (250,128,114),
    'darksalmon': (233,150,122),
    'lightcoral': (240,128,128),
    'indianred': (205,92,92),
    'crimson': (220,20,60),
    'firebrick': (178,34,34),
    'red': (255,0,0),
    'darkred': (139,0,0),
    'coral': (255,127,80),
    'tomato': (255,99,71),
    'orangered': (255,69,0),
    'gold': (255,215,0),
    'orange': (255,165,0),
    'darkorange': (255,140,0),
    'lightyellow': (255,255,224),
    'lemonchiffon': (255,250,205),
    'lightgoldenrodyellow': (250,250,210),
    'papayawhip': (255,239,213),
    'moccasin': (255,228,181),
    'peachpuff': (255,218,185),
    'palegoldenrod': (238,232,170),
    'khaki': (240,230,140),
    'darkkhaki': (189,183,107),
    'yellow': (255,255,0),
    'lawngreen': (124,252,0),
    'chartreuse': (127,255,0),
    'limegreen': (50,205,50),
    'forestgreen': (34,139,34),
    'green': (0,128,0),
    'darkgreen': (0,100,0),
    'greenyellow': (173,255,47),
    'yellowgreen': (154,205,50),
    'springgreen': (0,255,127),
    'mediumspringgreen': (0,250,154),
    'lightgreen': (144,238,144),
    'palegreen': (152,251,152),
    'darkseagreen': (143,188,143),
    'mediumseagreen': (60,179,113),
    'seagreen': (46,139,87),
    'olive': (128,128,0),
    'darkolivegreen': (85,107,47),
    'olivedrab': (107,142,35),
    'lightcyan': (224,255,255),
    'cyan': (0,255,255),
    'aqua': (0,255,255),
    'aquamarine': (127,255,212),
    'mediumaquamarine': (102,205,170),
    'paleturquoise': (175,238,238),
    'turquoise': (64,224,208),
    'mediumturquoise': (72,209,204),
    'darkturquoise': (0,206,209),
    'lightseagreen': (32,178,170),
    'cadetblue': (95,158,160),
    'darkcyan': (0,139,139),
    'teal': (0,128,128),
    'powderblue': (176,224,230),
    'lightblue': (173,216,230),
    'lightskyblue': (135,206,250),
    'skyblue': (135,206,235),
    'deepskyblue': (0,191,255),
    'lightsteelblue': (176,196,222),
    'dodgerblue': (30,144,255),
    'cornflowerblue': (100,149,237),
    'steelblue': (70,130,180),
    'royalblue': (65,105,225),
    'blue': (0,0,255),
    'mediumblue': (0,0,205),
    'darkblue': (0,0,139),
    'navy': (0,0,128),
    'midnightblue': (25,25,112),
    'mediumslateblue': (123,104,238),
    'slateblue': (106,90,205),
    'darkslateblue': (72,61,139),
    'lavender': (230,230,250),
    'thistle': (216,191,216),
    'plum': (221,160,221),
    'violet': (238,130,238),
    'orchid': (218,112,214),
    'fuchsia': (255,0,255),
    'magenta': (255,0,255),
    'mediumorchid': (186,85,211),
    'mediumpurple': (147,112,219),
    'blueviolet': (138,43,226),
    'darkviolet': (148,0,211),
    'darkorchid': (153,50,204),
    'darkmagenta': (139,0,139),
    'purple': (128,0,128),
    'indigo': (75,0,130),
    'pink': (255,192,203),
    'lightpink': (255,182,193),
    'hotpink': (255,105,180),
    'deeppink': (255,20,147),
    'palevioletred': (219,112,147),
    'mediumvioletred': (199,21,133),
    'white': (255,255,255),
    'snow': (255,250,250),
    'honeydew': (240,255,240),
    'mintcream': (245,255,250),
    'azure': (240,255,255),
    'aliceblue': (240,248,255),
    'ghostwhite': (248,248,255),
    'whitesmoke': (245,245,245),
    'seashell': (255,245,238),
    'beige': (245,245,220),
    'oldlace': (253,245,230),
    'floralwhite': (255,250,240),
    'ivory': (255,255,240),
    'antiquewhite': (250,235,215),
    'linen': (250,240,230),
    'lavenderblush': (255,240,245),
    'mistyrose': (255,228,225),
    'gainsboro': (220,220,220),
    'lightgray': (211,211,211),
    'silver': (192,192,192),
    'darkgray': (169,169,169),
    'gray': (128,128,128),
    'dimgray': (105,105,105),
    'lightslategray': (119,136,153),
    'slategray': (112,128,144),
    'darkslategray': (47,79,79),
    'black': (0,0,0),
    'cornsilk': (255,248,220),
    'blanchedalmond': (255,235,205),
    'bisque': (255,228,196),
    'navajowhite': (255,222,173),
    'wheat': (245,222,179),
    'burlywood': (222,184,135),
    'tan': (210,180,140),
    'rosybrown': (188,143,143),
    'sandybrown': (244,164,96),
    'goldenrod': (218,165,32),
    'peru': (205,133,63),
    'chocolate': (210,105,30),
    'saddlebrown': (139,69,19),
    'sienna': (160,82,45),
    'brown': (165,42,42),
    'maroon': (128,0,0)

}

# imagefile = "#path to image file" #######
# image = cv2.imread(imagefile)
# print(imagefile)
# image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

def remove_background(cv2_image_file):
    ############################################### Remove background starts here
    ## (1) Read
    img = cv2_image_file #cv2.imread(imagefile)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #print(img.shape)
    img = cv2.resize(img,(45,90))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    ## (2) Threshold
    th, threshed = cv2.threshold(gray, 235, 200, cv2.THRESH_BINARY_INV)

    ## (3) Find the first contour that greate than 100, locate in centeral region
    ## Adjust the parameter when necessary
    _, cnts, _ = cv2.findContours(threshed, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    cnts = sorted(cnts, key=cv2.contourArea)
    H,W = img.shape[:2]
    for cnt in cnts:
        x,y,w,h = cv2.boundingRect(cnt)
        if cv2.contourArea(cnt) > 3500 and (0.7 < w/h < 1.3) and (W/4 < x + w//2 < W*3/4) and (H/4 < y + h//2 < H*3/4):
            break

    ## (4) Create mask and do bitwise-op
    mask = np.zeros(img.shape[:2],np.uint8)
    cv2.drawContours(mask, [cnt],-1, 255, -1)
    image = cv2.bitwise_and(img, img, mask=mask)
    return image
    ################################################ ends here



def calculate_clusters(image):
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    # reshape the image to be a list of pixels
    #image_1 = image.reshape((image.shape[0] * image.shape[1], 3))

    image_1 = image.reshape((image.shape[0] * image.shape[1], 3))

    # cluster the pixel intensities

    clt = KMeans(n_clusters = 3)
    clt.fit(image_1)
    return clt

def centroid_histogram(clt):
    # grab the number of different clusters and create a histogram
    # based on the number of pixels assigned to each cluster
    numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
    (hist, _) = np.histogram(clt.labels_, bins = numLabels)
    
    # normalize the histogram, such that it sums to one
    hist = hist.astype("float")
    hist /= hist.sum()
    
    # return the histogram
    return hist


def plot_colors(hist, centroids):
    # initialize the bar chart representing the relative frequency
    # of each of the colors
    bar = np.zeros((50, 300, 3), dtype = "uint8")
    startX = 0
    
    # loop over the percentage of each cluster and the color of
    # each cluster
    colors = []
    for (percent, color) in zip(hist, centroids):
        # plot the relative percentage of each cluster
        #print(color.astype("uint8").tolist())
        endX = startX + (percent * 300)
        cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
            color.astype("uint8").tolist(), -1)
        colors.append(color.astype("uint8").tolist())
        startX = endX
    
    # return the bar chart
    return bar,colors,percent

# Filter Hintergrund-Schwarz raus (0,0,0) sowie farben die weniger als x% der Fläche abdecken

        
# build a histogram of clusters and then create a figure
# representing the number of pixels labeled to each color

# berechne histogram und filter relevante Farben raus
def run_histo_and_bar_calculation(clt):
    hist = centroid_histogram(clt)
    bar, colors,percent = plot_colors(hist, clt.cluster_centers_)
    percentage_of_colors = []
    filtered_colors = []
    for (percentage, color) in (zip(hist,colors)):
        print(percentage)
        if sum(color) >= 20 and percentage > 0.05:
            filtered_colors.append(color)
            percentage_of_colors.append(percentage)
    return filtered_colors, percentage_of_colors, bar
# plt.imshow(image)
# # show our color bart
# plt.figure()
# plt.axis("off")
# plt.imshow(bar)
# plt.show()




def find_closest_color(look_up):
    result_farbe = []
    distanzen = []
    for j,color_rgb in (zip(color_dict,color_dict.values())):

        
        # Convert from RGB to Lab Color Space
        color_lab = convert_color(sRGBColor(*color_rgb,is_upscaled=True), LabColor)
        lookup_lab = convert_color(sRGBColor(*look_up,is_upscaled=True), LabColor)


        # Find the color difference
        delta_e = delta_e_cie2000(color_lab, lookup_lab)
        distanzen.append(delta_e)
        result_farbe.append(j)
#         print(j)
#         print(color_rgb)
#         print(delta_e)
# finde die drei farben mit der kleinsten distanz zur gesuchten farbe
    min_3_dis_index = np.array(distanzen).argsort()[:3]
   # print(min_3_dis_index)
    c = [result_farbe[i] for i in min_3_dis_index]
    #print(c)
    return c


####################################
# Farbnamen bestimmen
def out_put_farbe(hist, filtered_colors):
    vip_farbe_index = np.argmax(hist,axis=-1)
    ## create image
    width = 60
    blank_image = np.zeros((60,60,3), np.uint8)
    blank_image[:60] = filtered_colors[vip_farbe_index]     # (B, G, R)
    #blank_image[:,0.5*width:width] = (0,255,0)
    #plt.imshow(blank_image)

    mean = cv2.mean(blank_image, mask=None)[:3]
    ergebnis = find_closest_color(mean)
    print("Gesuchte Farbe lautet: ", ergebnis)
    return ergebnis


# Führt alles aus! 
def run_farberkennung(cv2_image_file):
    masked_image = remove_background(cv2_image_file)
    clt = calculate_clusters(masked_image)
    #hist = centroid_histogram(clt)
    #bar, colors,percent = plot_colors(hist, clt.cluster_centers_)

    # RGB gefilterte Farbwerte (mehrere)
    filtered_colors, percentage_of_colors, bar = run_histo_and_bar_calculation(clt)
    # Aktuell wird nur die Farbe weitergeleitet die den größten Anteil im Cluster ausmacht (1/3)
    # gebe das %-wise wichtigste RBG-triplet weiter

    # Erhalte 3 NAMEN die am ehesten zur dem an find_clostet_color gegebenen RGB-Triplet passen
    #result_farben = find_closest_color(filtered_colors[np.argmax(percentage_of_colors,axis=-1)])
    ergebnis = out_put_farbe(percentage_of_colors, filtered_colors)
    return ergebnis, filtered_colors